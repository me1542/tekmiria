async function _getFile(url) {

    let response = await fetch(url, {
        method: 'GET',
    })
    let obj = await response.text()

    return obj
}

 async function myFunc() {
    
    let body = document.body;

    resp =  await _getFile('ergasia.xml')

    parser = new DOMParser();
    xmlDoc = parser.parseFromString(resp, "text/xml");
    console.log(xmlDoc)
    entries = xmlDoc.getElementsByTagName("entry")
    for (entry of entries) {
        let paragraph = document.createElement("p");
        //To λήμμα
        let lemma = entry.getElementsByTagName("orth")[0];
        let lemmaSpan = document.createElement("span");
        lemmaSpan.append(lemma.innerHTML + ": ");
        lemmaSpan.classList.add("lemma");
        paragraph.append(lemmaSpan);

        //Καταλήξεις γραμματικών τύπων (αν υπάρχουν)
        let suffix = entry.querySelector("orth[type='suffix']");
        let suffixSpan = document.createElement("span");
        suffixSpan.append(suffix ? "-" + suffix.innerHTML + ", " : "");
        suffixSpan.classList.add("suffix")
        paragraph.append(suffixSpan);
        //Το γένος του λήμματος (αν υπάρχει)
        let gender = entry.getElementsByTagName("gen");
        let genderSpan = document.createElement("span");
        genderSpan.append(gender[0] ? gender[0].innerHTML + ", " : "");
        genderSpan.classList.add("gender");
        paragraph.append(genderSpan);
        //ετυμολογία (αν υπάρχει)
        let etym = entry.getElementsByTagName("etym");
        let etymSpan = document.createElement("span");
        etymSpan.append(etym[0] ? "etym: " + etym[0].innerHTML + ", "  : "");
        etymSpan.classList.add("etym");
        paragraph.append(etymSpan);
        //συνώνυμα (αν υπάρχουν)
        let syn = entry.querySelector("usg[type='syn']");
        let synSpan = document.createElement("span");
        synSpan.append(syn ? "see also:" + syn.innerHTML: "");
        synSpan.classList.add("syn")
        paragraph.append(synSpan);
        //ορισμός (αν υπάρχει)
        let senses = entry.getElementsByTagName("sense"); //parse every sense
        let senseDiv = document.createElement("div");
        senseDiv.classList.add("sense");
        if (senses.length==0){
            let defDiv = document.createElement("div");
            defDiv.classList.add("definition");
            let defs = entry.getElementsByTagName("def"); //μία έννοια (sense) μπορεί να έχει πολλά definitions
            for (definition of defs) {
                defDiv.append("Definition "+":");
                defDiv.append(definition.innerHTML);
                defDiv.append(document.createElement('br'));
            }
            senseDiv.append(defDiv);
            let refs = entry.getElementsByTagName("ref");
            let refDiv = document.createElement("div");
            refDiv.classList.add("references");
            if (refs.length != 0) {
                refDiv.append("References: ")
                for (ref of refs) {
                    refDiv.append(ref.innerHTML + ", ");
                }
                senseDiv.append(refDiv);
            }
            paragraph.append(senseDiv);
        }
        for (sense of senses) {
            

            let defDiv = document.createElement("div");
            defDiv.classList.add("definition");
            defDiv.append("Definition " + sense.getAttribute("n") + ":");
            let defs = sense.getElementsByTagName("def"); //μία έννοια (sense) μπορεί να έχει πολλά definitions
            for (definition of defs) {
                defDiv.append(definition.innerHTML);
                defDiv.append(document.createElement('br'));
            }
            senseDiv.append(defDiv);

            let refs = sense.getElementsByTagName("ref");
            let refDiv = document.createElement("div");
            refDiv.classList.add("references");
            if (refs.length != 0) {
                refDiv.append("References: ")
                for (ref of refs) {
                    refDiv.append(ref.innerHTML + ", ");
                }
                senseDiv.append(refDiv);
            }


            paragraph.append(senseDiv);
        }
        
        body.append(paragraph);
        
    }

}
